'use strict';
import React, { useState } from 'react';
import ReactDom from 'react-dom';
import Moment from 'moment';
import JsonViewer from './../../src/js/index';

const Index = () => {
    const [json, setJson] = useState({
        "__component": "aarogya-page.call-button",
        "id": 11,
        "Header": "Don't settle for only health insurance!",
        "Title": "TOTAL HEALTHCARE IN EASY EMI",
        "SubTitle": "Get Health benefits + Insurance in Easy EMIs",
        "BackgroundColor": null,
        "TextColor": null,
        "Type": "Basic",
        "MetaData": {
            "button": {
                "cta": "Get Personalised Quote",
                "color": "#ed753f",
                "onClick": {
                    "eventAction": "get_personalised_quote",
                    "eventCategory": "aarogya_Care_lead_gen_without_form",
                    "eventLabel9": "en"
                }
            },
            "array": [{ "id": "1" }, { "id": "2" }],
            "disclaimer": "Powered by Bajaj Allianz General Insurance Company"
        }
    })

    return (
        <JsonViewer
            sortKeys
            style={{ padding: '30px', backgroundColor: 'white' }}
            src={json}
            quotesOnKeys={false}
            collapseStringsAfterLength={12}
            onEdit={e => {
                console.log('edit callback', e);
                setJson(e.updated_src)
                if (e.new_value == 'error') {
                    return false;
                }
            }}
            // onDelete={e => {
            //     console.log('delete callback', e);
            // }}
            onAdd={e => {
                console.log('add callback', e);
                setJson(e.updated_src)
                if (e.new_value == 'error') {
                    return false;
                }
            }}
            onApiCall={e => {
                console.log('api callback', e);
                if (e.new_value == 'error') {
                    return false;
                }
                setJson((prevData) => {
                    let updated_src = { ...e.updated_src };
                    let walk = updated_src;
                    for (const idx of e.namespace) {
                        walk = walk[idx];
                    }
                    walk[e.name] = "*******"
                    console.log("updated_src", updated_src);
                    console.log("walk", walk);
                    return updated_src;
                })
            }}
            onSelect={e => {
                console.log('select callback', e);
                console.log(e.namespace);
            }}
            displayObjectSize={true}
            name={'dev-server'}
            enableClipboard={copy => {
                console.log('you copied to clipboard!', copy);
            }}
            shouldCollapse={({ src, namespace, type }) => {
                if (type === 'array' && src.indexOf('test') > -1) {
                    return true;
                } else if (namespace.indexOf('moment') > -1) {
                    return true;
                }
                return false;
            }}
            defaultValue=""
        />
    )
}

ReactDom.render(<Index />, document.getElementById('app-container'))